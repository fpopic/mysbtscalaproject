lazy val root = (project in file(".")).settings(
  organization := "com.fp",
  name := "MySbtScalaProject",
  version := "1.0",
  sbtVersion := "0.13.12",
  scalaVersion := "2.11.8", // for %% operator
  parallelExecution in Test := true
)

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.6.2",
  "org.apache.spark" %% "spark-sql" % "1.6.2",
  "org.apache.spark" %% "spark-streaming" % "1.6.2",
  "org.apache.spark" %% "spark-streaming-kafka" % "1.6.2",

  "org.scalactic" %% "scalactic" % "3.0.0",
  "org.scalatest" %% "scalatest" % "3.0.0" % "test",
  "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % "test",

  "com.microsoft.sqlserver" % "sqljdbc4" % "4.0",
  "org.elasticsearch" %% "elasticsearch-spark" % "2.3.4",
  "com.databricks" %% "spark-csv" % "1.4.0"
)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case x => MergeStrategy.first
}

resolvers += "nexus" at "https://artifacts.alfresco.com/nexus/content/groups/public/"
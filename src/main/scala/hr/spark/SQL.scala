package hr.spark

import com.typesafe.config.ConfigFactory
import org.apache.spark.sql.{DataFrame, SQLContext, SaveMode}

trait SQLServer {

  def url : String

  def readTable(sparkSQL : SQLContext, table : String) : DataFrame

  def writeTable(dataFrame : DataFrame, table : String, mode : SaveMode) : Unit

}

class MSSQLServer(path : String, instance : String) extends SQLServer {

  val url : String = {
    // configuration database instance in /src/main/resources/path
    val conf = ConfigFactory.load(path).getObject(instance).unwrapped
    s"""jdbc:sqlserver://${conf.get("host")}
        |\\$instance:${conf.get("port")}
        |;databaseName=${conf.get("dbName")}
        |;user=${conf.get("user")}
        |;password=${conf.get("password")}""".stripMargin
  }

  private val emptyProperties = new java.util.Properties

  def readTable(sparkSQL : SQLContext, table : String) : DataFrame = {
    sparkSQL.read.jdbc(url, table, emptyProperties)
  }

  def writeTable(dataFrame : DataFrame, table : String, mode : SaveMode = SaveMode.Overwrite) : Unit = {
    dataFrame.write.mode(mode).jdbc(url, table, emptyProperties)
  }

}
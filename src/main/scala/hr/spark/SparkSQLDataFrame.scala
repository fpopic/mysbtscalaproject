
package hr.spark

import org.apache.spark.sql._
import org.apache.spark.{SparkConf, SparkContext}

// ako su mi tablice i case clasee identicni (tipovi i poredak) ne moras select stupaca
case class Person(Id : Int, Name : String, Age : Int, Car_Id : Option[Int])
case class Car(Id : Int, Name : String)
case class PersonWithExtras(Id : Int, extras : Option[Map[String, String]])

object SparkSQLDataFrame {

  def main(args : Array[String]) : Unit = {

    val sparkContext = new SparkContext(new SparkConf().setAppName("SparkMy").setMaster("local[*]"))
    val sparkSQL = new SQLContext(sparkContext)

    import sparkSQL.implicits._

    val dso = new MSSQLServer("mssql.json", "MSSQLSERVER")

    val persons = dso.readTable(sparkSQL, "Person").as[Person].cache()
    val cars = dso.readTable(sparkSQL, "Car").as[Car].cache()

    val personDF = persons.toDF
    val carDF = cars.toDF

    val fullPersons = personDF
      .join(carDF, 'Car_Id === carDF.col("Id")) // left join
      .drop(personDF.col("Car_Id")) //removing FK from left
      .drop(carDF.col("Id")) //removing PK from right

    fullPersons.show()
  }

}

package hr.spark

import java.sql.Timestamp

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}


case class EnclosingCaseClass(Events_Id : Option[Long], Animal_Id : Option[Long], Farm_Id : Option[Long], Farmer_Id : Option[Long], Time : Timestamp, EventName : String, Extras : Option[Extras])

case class Extras(From_Stall_Id : Option[Long] = None, To_Stall_Id : Option[Long] = None, HoldingPen_Id : Option[Long] = None)

object ElasticSearch {

  def main(args : Array[String]) : Unit = {

    val esOptions = Map(
      "spark.sql.shuffle.partitions" -> "4",
      "es.nodes" -> "localhost",
      "es.port" -> "9200",
      "es.index.auto.create" -> "yes")

    val sparkContext = new SparkContext(
      new SparkConf().setAppName("ElasticSearch").setMaster("local[*]").setAll(esOptions))
    val sparkSQL = new SQLContext(sparkContext)

    import sparkSQL.implicits._

    //    events.saveAsPrecomputedView("xxx/activity_log")
    //
    //    import org.elasticsearch.spark.sql._
    //    val rawEvents = sparkSQL.esDF("xxx/activity_log").parseExtrasToMap(sparkSQL).as[RawEvent]
    //    rawEvents.printSchema()
    //    rawEvents.show(truncate = false)
  }

}

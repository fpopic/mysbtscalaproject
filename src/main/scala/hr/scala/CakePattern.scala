package hr.scala

import java.nio.ByteBuffer

//good practise trait for package which shows how to wire objects (recommendend)

object CakePattern extends App {
  case class Field()
  case class Digger()
  case class PotatoFarm(field : Field, digger : Digger)

  case class CowPasture()
  case class Meatery(cowPasture : CowPasture)

  case class Restaurant(potatoFarm : PotatoFarm, meatery : Meatery) {
    def orderStakeWithPotatoes() = println(s"Welcome to $this. Here's your order!")
  }

  trait CropModule {
    //singletons, static shared instances
    lazy val field = Field()
    lazy val digger = Digger()

    // at each use, new PotatoFarm instance
    def potatoFarm = PotatoFarm(field, digger)
  }

  trait LiveStockModule {
    lazy val cowPasture = CowPasture()
    lazy val meatery = Meatery(cowPasture)
  }

  trait RestaurantModule extends CropModule with LiveStockModule {
    lazy val restaurant = Restaurant(potatoFarm, meatery)
  }

  val appCake : RestaurantModule with CropModule = new RestaurantModule with CropModule
  appCake.restaurant.orderStakeWithPotatoes()

}


// top-level trait-modules
// instead of import => extends,with

//programmer doesn't know implementation of User
//and even the type of User
trait UserModule {

  //abstract
  def login(name : String, pass : String) : Option[User]

  //abstract
  def save(user : User) : Unit

  //abstract type User constrained to extend UserLike (abstract class)
  type User <: UserLike

  trait UserLike {
    this : User =>

    def id : String

    def name : String

    def save() : Unit
  }

  //abstract constructor
  def User(id : String, name : String) : User

}

trait LifeCycle {

  def startup() : Unit

  def shutdown() : Unit

}

trait MongoUserModule extends UserModule with LifeCycle {

  //abstract override allows super. pointer
  abstract override def startup() : Unit = {
    super.startup()
    //initMongoThingy()
  }

  abstract override def shutdown() : Unit = {
    //killMongoThingy()
    super.shutdown()
  }

  //impl
  class User extends UserLike {

    def save() : Unit = {

    }

    def User(id : String, name : String) : User = {
      //      new User(id, name)
      null
    }

    def id : String = ???

    def name : String = ???
  }


  def login(user : String, pass : String) = {
    null
  }

}


//only depends on abstract module, not implementation!
trait MessageModule extends UserModule {

  //object Message is just to disinguish save from UserModule save (to avoid overloading)
  //object namespacing rather than packaging
  object Message {
    def render(msg : Message) {

    }

    def save(msg : Message) {

    }
  }
  case class Message(author : User, body : String)
}


trait StorageModule {
  def store(id : Long, data : ByteBuffer)

  def retrive(id : Long) : ByteBuffer
}


trait SysemModule extends StorageModule {
  val userModule : UserModule

  def doStuff(userModule : UserModule) = {
//    userModule.makeUsers()
//    userModule.encourageMemes()
  }

}
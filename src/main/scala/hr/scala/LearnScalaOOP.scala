package hr.scala

import scala.collection.mutable

object LearnScalaOOP {

    trait MyTrait1 {
        val notDefined : String
    }

    trait MyTrait2 {
        val notDefined : String
    }

    trait MyTrait3 {
        val a : Int = 5
    }

    abstract class MyAbstractClass(needed : Int) {
        def f1(x : Int) : Int

        def f2() : String

        def f3() : String


    }

    //class with it's 1.st constructor
    class MyClass(hidden : String, val accessable : String, val needed : Int)
      extends MyAbstractClass(needed) with MyTrait1 with MyTrait2 {
        // ako zelim javin get/set @BeanProperty dodaj ispred val u konstruktoru

        println("Inside the primary constructor")

        val publicField : String = if (hidden == "Ducky") "red" else "yellow"

        //2.nd constructor
        def this(a : Int) {
            this(a.toString, "I am the encapsulated string.", 1)
            println("Inside the secondary constructor")
        }

        override def toString : String = "I am toString of object"

        override def f1(x : Int) : Int = x + 1

        override def f2() : String = ???

        override def f3() : String = ???

        override val notDefined : String = ""
    }

    object MyClass {
        val MY_STATIC_VARIABLE : String = "I am static string"
    }

    // CASE CLASS
    // auto adds 'val',
    // toString, equals, hashcode
    // creates class with it's companion object
    // implements apply()
    // ommits 'new' keyword
    // generates 'copy()'
    case class MyCaseClass(age : Int, gender : String)

    def MyDataClassType(myDataClass : MyCaseClass) = myDataClass match {
        case MyCaseClass(12, "Male") => "it's marko"
        case MyCaseClass(age, gender) => "it's not marko"
    }

    class MyGenericClass[GenericType1](type1 : GenericType1)

    def main(args : Array[String]) {
        val myClassObject = new MyClass(needed = 2, hidden = "ducky", accessable = "dd")
        println(MyClass.MY_STATIC_VARIABLE)
        val a = MyDataClassType(MyCaseClass(11, "s"))
        println(a)
        new MyClass(12)

        myClassObject accessable 1

        val my2ClassObject =
            new MyClass(needed = 1, hidden = "1", accessable = "1") with MyTrait3 {override val a : Int = 555}


        val myList = mutable.MutableList(3, 4, 5)

        myList += 1

        println(myList)

    }

}
